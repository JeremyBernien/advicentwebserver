# Advicent School web server

## Getting Started

To install Go: <https://golang.org/doc/install>: 
If using Windows, make sure to set the Go path environment variable

Clone the project with

```
git clone git@bitbucket.org:JeremyBernien/advicentwebserver.git
```


### Installing

To run this project from within /src/app:

```
go run *.go
```

Some sample requests:
```
http://localhost:8080/college/cost?collegeName=Adelphi%20University
http://localhost:8080/college/cost?collegeName=Adelphi%20University&includeRoomAndBoard=true
http://localhost:8080/college/cost?collegeName=Adelphi%20University&includeRoomAndBoard=false
```

## Running the tests
```
go test
```

## Built With

* [gin-gonic](https://github.com/gin-gonic/gin)

## Versioning

[SemVer](http://semver.org/) for versioning.

## Authors

**Jeremy Bernien**

## License

This project is unlicensed.

