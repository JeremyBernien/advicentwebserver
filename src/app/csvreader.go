package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

// TODO: Correctly error handle csv file open and line read
func parseCSV(fileName string) []College {
	csvFile, err := os.Open(fileName)
	if err != nil {
		fmt.Println("CSV Reader was not able to open the file: " + fileName)
		log.Fatal(err)
	}

	reader := csv.NewReader(bufio.NewReader(csvFile))
	var colleges []College

	// Throw away the headers
	reader.Read()
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Error reading line: " + line[0])
			log.Fatal(err)
		}

		colleges = append(colleges, parseLine(line))
	}
	return colleges
}

// TODO: Correctly error handle line parsing (str to int)
// TODO: It would be faster to create a map, rather than iterate and compare values
func parseLine(line []string) College {
	collegeName := string(line[0])
	instateTuition, _ := strconv.ParseFloat(string(line[1]), 64)
	outStateTutition, _ := strconv.ParseFloat(string(line[2]), 64)
	roomAndBoard, _ := strconv.ParseFloat(string(line[3]), 64)

	return College{
		CollegeName:       collegeName,
		InstateTuition:    instateTuition,
		OutOfStateTuition: outStateTutition,
		RoomAndBoard:      roomAndBoard,
	}
}
