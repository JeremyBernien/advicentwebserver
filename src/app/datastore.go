package main

import (
	"strings"
)

// College is a model type representing a college entry
type College struct {
	CollegeName       string  `json:"CollegeName"`
	InstateTuition    float64 `json:"InstateTuition"`
	OutOfStateTuition float64 `json:"OutOfStateTuition"`
	RoomAndBoard      float64 `json:"RoomAndBoard"`
}

type csvDatastore struct {
	data []College
}

func (csv csvDatastore) getCollege(collegeName string) (College, error) {
	var collegeResult College

	for _, college := range csv.data {
		if strings.EqualFold(college.CollegeName, collegeName) {
			collegeResult = college
		}
	}
	return collegeResult, nil
}
