package main

import (
	"github.com/gin-gonic/gin"
)

// TODO: Unit tests and e2e tests
// TODO: Provide interface to query csv result as SQL to make datastore interchangeable
// TODO: Provide swagger definition (https://goswagger.io/install.html)
// TODO: Security vulernabilities: add unimplemented error for POST, parameter sanitization, auth needed in future?
func main() {
	router := gin.Default()
	collegeRouter := router.Group("college")
	collegeRouter.GET("/cost", costHandler)

	router.Run(":8080")
}
