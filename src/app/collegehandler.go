package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

var datastore Datastore = csvDatastore{data: parseCSV("./../colleges.csv")}

// Datastore is an interface to provide an abstraction to the datastores
type Datastore interface {
	getCollege(collegeName string) (College, error)
}

// costHandler handles routes from /college/cost
func costHandler(c *gin.Context) {
	collegeName := c.Query("collegeName")
	roomAndBoardIncluded := c.DefaultQuery("includeRoomAndBoard", "true")

	if len(collegeName) < 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "College name is required"})
		return
	}

	collegeResult, err := datastore.getCollege(collegeName)
	if err != nil || len(collegeResult.CollegeName) < 1 {
		// TODO: Requirements called for an error response on college not found (not 204?)
		c.JSON(http.StatusNotFound, gin.H{"error": "College not found"})
		return
	}

	total := calculateCost(collegeResult, roomAndBoardIncluded, true)

	c.JSON(http.StatusOK, gin.H{"data": fmt.Sprintf("%.2f", total)})
	return
}

func calculateCost(college College, roomAndBoardIncluded string, inState bool) float64 {
	var total float64

	if inState {
		total = college.InstateTuition
	} else {
		total = college.OutOfStateTuition
	}

	includeRoomAndBoard, _ := strconv.ParseBool(roomAndBoardIncluded)
	if includeRoomAndBoard {
		total += college.RoomAndBoard
	}
	return total
}
